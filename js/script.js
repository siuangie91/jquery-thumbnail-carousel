$(document).ready(function() {
    var totalWidth = 0; //the combined width of all images
    var positions = []; 
    
    //loop thru slides 
    $('#slides .slide').each(function(i) {
        //traverse thru slides and store widths in totalWidth
        positions[i] = totalWidth;
        totalWidth += $(this).width();
        
        //make sure that the current img has a set width
        if(!$(this).width()) {
            alert('Please add a width to your images');
            return false;
        }
    });
    
    //set slides div's width to totalWidth
    $('#slides').width(totalWidth);
    
    //menu item click handler
    $('#menu ul li a').click(function(e, keepScroll) {
        //remove active class and add inactive class
        $('li.product').removeClass('active').addClass('inactive');
        // add active class to parent 
        $(this).parent().addClass('active');
        
        //set pos to how many previous siblings with the class 'product' that the current a's parent li has 
        var pos = $(this).parent().prevAll('.product').length;
        
        $('#slides').stop().animate({marginLeft: -positions[pos] + 'px'}, 10000);
        
        //prevent default
        e.preventDefault();
        
        //stop autoScroll
        if(!autoScroll) clearInterval(itvl);
    });
    
    //make first image active
    $('#menu ul li.product:first').addClass('active').siblings().addClass('inactive');
    
    //autoScroll
    var current = 2;
    function autoScroll() {
        if(current == -1) {
            return false;    
        }
        $('#menu ul li a').eq(current % $('#menu ul li a').length).trigger('click', [true]);
        current++;
    }
    
    //duration for autoScroll
    var duration = 10; //in seconds
    var itvl = setInterval(function() {
        autoScroll();
    }, duration * 1000);

});











